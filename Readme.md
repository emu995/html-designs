# HTML Design-Studien

HTML Design-Studien für einzelne Elemente (z.B. Buttons, Tabellen, ...) auf hellen und dunklen Hintergründen und Themes mit Login- und Wiki-Seite.

## Installation

Nach der Installation von Sass über `sudo npm install -g sass` müssen die Sass-Dateien einmalig kompiliert werden:

    sass --watch .

Danach kann die Datei `index.html` geöffnet werden.

## Ideen

- Weitere Formular-Elemente: Drop Down-Menü, Checkbox, Formular-Validierung
- Button mit Lade-Animation bei Klick (vgl. comdirect)
