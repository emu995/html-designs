function toggleOverlay() {
    document.getElementById('progressBar').classList.toggle('visible')
}

window.onload = function () {
    document.getElementById('toggleOverlayButton').addEventListener('click', toggleOverlay)
}
